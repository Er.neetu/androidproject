package com.example.greetingcard

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Row
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import com.example.greetingcard.ui.theme.GreetingCardTheme
import androidx.compose.foundation.layout.Column



class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            GreetingCardTheme {
                // A surface container using the 'background' color from the theme
                Surface(
//                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    BirthdayGreetingWithText("hi", text = "nayu")
                }
            }
        }
    }
}

@Composable
fun BirthdayGreetingWithText(message: String, text: String) {
    Column{
        Text(
            text = message,
            fontSize = 36.sp
        )

        Text(
            text = text,
            fontSize = 20.sp
        )
    }
}

@Preview(showBackground = false)
@Composable
fun BirthdayCardPreview() {
    GreetingCardTheme {
        BirthdayGreetingWithText("happy Birthday neetu!","-from nayra")
    }
}